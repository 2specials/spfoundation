import XCTest

import SPFoundationTests

var tests = [XCTestCaseEntry]()
tests += SPFoundationTests.allTests()
XCTMain(tests)
