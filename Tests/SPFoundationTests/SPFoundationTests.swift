import XCTest
@testable import SPFoundation

final class SPFoundationTests: XCTestCase {
    
    func testIntegerStringConversion() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        let n: UInt8 = 100
        let resultString = n.stringValue
        XCTAssertEqual(resultString, "100")
    }

    static var allTests = [
        ("integerStringConversion", testIntegerStringConversion)
    ]
    
}
