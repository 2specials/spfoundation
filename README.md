# SPFoundation
This repository contains a set of useful utilities about Apple development based on Apple Foundation framework.

The documentation can be found at https://2specials.gitlab.io/spfoundation.

To generate documentation type the following command:
`jazzy --config .jazzy.json`