
import Foundation

public struct SPFTableModel {
    var sections: [SPFTableModelSection]
}

public  struct SPFTableModelSection : SPFGeneralType {
    let type: SPFTableModelSectionType?
    let header: SPFTableModelSectionHeader?
    var items: [SPFTableModelSectionItem]
    let footer: SPFTableModelSectionFooter?
}

public protocol SPFTableModelSectionItem {
    
}

public protocol SPFTableModelSectionType {
    
}

public protocol SPFTableModelSectionHeader {
    
}

public protocol SPFTableModelSectionFooter {
    
}
