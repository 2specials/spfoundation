
import Foundation

public extension Bundle {
    
    func getResources(name:String, fileExtension:String) -> Data? {
        if let file = Bundle.main.url(forResource: name, withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: file)
                return data
            } catch {
                SPFUtility.log(message: error.localizedDescription)
            }
        }
        return nil
    }
    
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    /**
     Gets the contents of the specified plist file.
     
     - parameter plistName: property list where defaults are declared
     - parameter bundle: bundle where defaults reside
     
     - returns: dictionary of values
     */
    static func contentsOfFile(plistName: String) -> [String : AnyObject] {
        let fileParts = plistName.components(separatedBy: ".")
        guard fileParts.count == 2,
            let resourcePath = Bundle.main.path(forResource: fileParts[0], ofType: fileParts[1]),
            let contents = NSDictionary(contentsOfFile: resourcePath) as? [String : AnyObject]
            else { return [:] }
        return contents
    }
    
//    public static func loadView<T>(fromNib name: String, withType type: T.Type) -> T? {
//        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
//            return view
//        }
//        return nil
//    }
    
}
