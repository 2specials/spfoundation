
import Foundation
import UIKit

public extension String {
    
    func toDictionary() -> [String: Any]? {
        if let jsonData = self.data(using: .utf8) {
            let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            return jsonObject as? [String: Any]
        }
        return nil
    }
    
    func toJSONObject() -> Any? {
        if let jsonData = self.data(using: .utf8) {
            let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            return jsonObject
        }
        return nil
    }
    
    var isEmail: Bool {
        let result:Bool = self.checkIfRegexIsValid(string:"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return result
    }
    
    func checkIfRegexIsValid(string:String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: string, options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    func intValue() -> Int? {
        return Int(self)
    }
    
    func int64Value() -> Int64? {
        return Int64(self)
    }
    
    func floatValue() -> Float? {
        return Float(self)
    }
    
    var boolValue: Bool? {
        switch self.lowercased() {
        case "true", "t", "yes", "y", "1":
            return true
        case "false", "f", "no", "n", "0":
            return false
        default:
            return nil
        }
    }
    
    // See: https://stackoverflow.com/questions/26797739/does-swift-have-a-trim-method-on-string
    // To remove all the whitespace from the beginning and end of a String
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    // See: https://stackoverflow.com/questions/28182441/swift-how-to-get-substring-from-start-to-last-index-of-character/28182819
    //right is the first encountered string after left
    func between(_ left: String, _ right: String) -> String? {
        guard
            let leftRange = range(of: left), let rightRange = range(of: right, options: .backwards)
            , leftRange.upperBound <= rightRange.lowerBound
            else { return nil }
        
        let sub = self[leftRange.upperBound...]
        let closestToLeftRange = sub.range(of: right)!
        return String(sub[..<closestToLeftRange.lowerBound])
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    func character(_ at: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: at)]
    }
    
    func stringIfNotEmpty() -> String? {
        return !self.isEmpty ? self : nil
    }
    
    var isHtml: Bool {
        let result:Bool = self.checkIfRegexIsValid(string:"<(\"[^\"]*\"|'[^']*'|[^'\">])*>")
        return result
    }
    
    /// Decimal representation of given binary string.
    var fromBinaryToDecimal: Int? {
        let result = Int(self, radix: 2)
        return result
    }
    
    /// Decimal representation of given hexadecimal string.
    var fromHexadecimalToDecimal: Int? {
        let result = Int(self, radix: 16)
        return result
    }
    
    /// Hexadecimal representation of given binary string.
    var fromBinaryToHexadecimal: String? {
        guard let binaryString = Int(self, radix: 2) else {
            return nil
        }
        let result = String(binaryString, radix: 16)
        return result
    }
    
    /// Binary representation of given hexadecimal string.
    var fromHexadecimalToBinary: String? {
        guard let integerValue = Int(self, radix: 16) else {
            return nil
        }
        let result = String(integerValue, radix: 2)
        return result
    }
    
    static func createAttributedString(strings: [String],
                       boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont.systemFont(ofSize: 14),
                       color: UIColor = UIColor.black) -> NSAttributedString {
        let attributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: color]
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: attributes)
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont,
                                 NSAttributedString.Key.foregroundColor: boldColor]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute,
                                           range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
}
