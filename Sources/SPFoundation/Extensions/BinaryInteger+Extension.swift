
import Foundation

public extension BinaryInteger {
    
    var stringValue: String {
        return String(self)
    }
    
    var floatValue: Float {
        return Float(self)
    }
    
    /// Binary representation of given decimal.
    var toBinaryString: String {
        let result = String(self, radix: 2)
        return result
    }
    
    /// Hexadecimal representation of given decimal.
    var toHexadecimalString: String {
        let result = String(self, radix: 16)
        return result
    }
    
}
