
import Foundation

public extension Dictionary {
 
    func toJSONString() -> String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: []) {
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        }
        return nil
    }
    
    mutating func merge(dict: [Key: Value]) {
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
    
}
