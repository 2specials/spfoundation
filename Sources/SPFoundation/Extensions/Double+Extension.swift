
import Foundation

public extension Double {
 
    var intValue: Int {
        return Int(self)
    }
    
}
