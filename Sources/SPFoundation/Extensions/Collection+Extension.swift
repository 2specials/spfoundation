
import Foundation

public extension Collection {
    
    /**
     Returns the element at the specified index if it's within bounds, otherwise nil.
     */
    subscript (safe index: Index) -> Element? {
        guard self.indices.contains(index) else { return nil }
        return self[index]
    }
    
}
