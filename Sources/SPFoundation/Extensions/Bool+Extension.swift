
import Foundation

public extension Bool {
    
    var toString: String {
        return String(self)
    }
    
    var intValue: Int {
        return self ? 1 : 0
    }
    
}
