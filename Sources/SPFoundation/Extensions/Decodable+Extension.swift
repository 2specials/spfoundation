
import Foundation

public extension Decodable {
 
    init(from: Any) throws {
        let data = try JSONSerialization.data(withJSONObject: from, options: .prettyPrinted)
        let decoder = JSONDecoder()
        self = try decoder.decode(Self.self, from: data)
    }
    
    init(fromPlistResource: String,
         in bundle: Bundle = Bundle.main) throws {
        let path = bundle.path(forResource: fromPlistResource,
                               ofType: "plist")
        if let path = path,
            let xml = FileManager.default.contents(atPath: path) {
            let decoder = PropertyListDecoder()
            self = try decoder.decode(Self.self,
                                      from: xml)
            return
        }
        throw SPFDecodableError.plistResourceNotFound
    }
    
}
