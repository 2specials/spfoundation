
import Foundation

public extension Date {
 
    static func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    static func dateFromString(_ dateString: String,
                               format: String,
                               timezone: TimeZone? = TimeZone(secondsFromGMT: 0)) -> Date? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format
        dateFormatterGet.timeZone = timezone
        let startDate = dateFormatterGet.date(from: dateString)
        return startDate
    }
    
    func toString(with format: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        let string = dateFormatterPrint.string(from: self)
        return string
    }
    
    static func dateFrom(timezone: TimeZone? = TimeZone(secondsFromGMT: 0),
                         format: String) -> Date? {
        guard let timezone = timezone else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timezone
        let dateString = dateFormatter.string(from: Date())
        return dateFormatter.date(from:dateString)
    }
    
}
