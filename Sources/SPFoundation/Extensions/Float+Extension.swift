
import Foundation

public extension Float {
 
    var stringValue: String {
        return String(self)
    }
    
    var intValue: Int {
        return Int(self)
    }
    
}
