
import Foundation

public extension Sequence where Iterator.Element == Bool {
    
    var isFullTrue:Bool {
        return !self.contains(false)
    }

    var isFullFalse:Bool {
        return !self.contains(true)
    }
    
}

public extension Sequence where Element == String {
    
    var pathString:String {
        return self.filter { !$0.isEmpty }.joined(separator: "/")
    }
    
}
