
import Foundation

public extension Array where Element : Equatable {
    
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
    
}

public extension Array {
    
    func toJSONString() -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
}

public extension Array where Element : StringProtocol {
    
    // See: https://stackoverflow.com/questions/25827033/how-do-i-convert-a-swift-array-to-a-string
    func toStringWithSeparator(separator: String) -> String {
        return self.joined(separator: separator)
    }
    
}
