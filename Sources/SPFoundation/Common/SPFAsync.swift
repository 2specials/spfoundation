
import Foundation

/**
 Struct is a shortcut for async operations
 */
public struct SPFAsync {
    
    /**
     Executes the given closure asyncronously in the main thread
     */
    public static func main(_ completion: @escaping @convention(block) () -> Swift.Void) {
        DispatchQueue.main.async { completion() }
    }
    
    /**
     Executes the given closure asyncronously in the main thread, after a dispatched time
     */
    public static func main(after: DispatchTime, _ completion: @escaping @convention(block) () -> Swift.Void) {
        DispatchQueue.main.asyncAfter(deadline: after) { completion() }
    }
    
    /**
     Executes the given closure asyncronously in a background thread
     */
    public static func background(_ completion: @escaping @convention(block) () -> Swift.Void) {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async { completion() }
    }
    
    /**
     Executes the given closure asyncronously in a background thread, after a dispatched time
     */
    public static func background(after: DispatchTime, _ completion: @escaping @convention(block) () -> Swift.Void) {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).asyncAfter(deadline: after) { completion() }
    }
    
}
