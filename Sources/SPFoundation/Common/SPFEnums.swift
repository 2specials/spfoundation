
import Foundation

public enum SPFControllerPresentationMode: String {
    case root = "root"
    case modal = "modal"
    case push = "push"
}

public enum SPFControllerClosingMode: String {
    case popToRoot = "popToRoot"
    case pop = "pop"
    case dismiss = "dismiss"
}

public enum SPFUrlScheme: String {
    case maps = "maps://"
    case googleMaps = "googlemaps://"
    case waze = "waze://"
}

/// Enum listing reasons that a resource manipulation could fail.
public enum SPFDecodableError: Error {
    /// The resources couldn't be found.
    case plistResourceNotFound
}
