
import Foundation

enum SPFVersionError: Error {
    case invalidResponse, invalidBundleInfo
}

public class SPFUtility {
    
    public class func log(message: Any,
                          filePath: String = #file,
                          lineNumber: Int = #line,
                          functionName: String = #function,
                          withNSLog: Bool = false) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let strDate = formatter.string(from: Date())
        let fileName = filePath.components(separatedBy: "/").last!.components(separatedBy: ".").first!
        print("\(strDate) -> \(fileName):\(lineNumber) - \(functionName) - \(message)")
        if withNSLog {
            NSLog("\(strDate) -> \(fileName):\(lineNumber) - \(functionName) - \(message)")
        }
        // debugPrint("\(strDate) -> \(fileName):\(lineNumber) - \(functionName) - \(message)")
    }
    
    public class func pingHost(_ url: String,
                               timeout: TimeInterval = 3.0,
                               _ completion: @escaping (Bool) -> Swift.Void) {
        guard let urlConverted = URL(string: url) else {
            completion(false)
            return
        }
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = timeout
        sessionConfig.timeoutIntervalForResource = timeout
		sessionConfig.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: sessionConfig)
        let task = session.dataTask(with: urlConverted) { _, response, _ in
            completion(response != nil)
        }
        task.resume()
    }
    
    /*
    public class func isUpdateAvailable(_ bundle: Bundle = Bundle.main) throws -> Bool {
        guard let currentVersion = bundle.releaseVersionNumber,
            let identifier = bundle.bundleIdentifier,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw SPFVersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw SPFVersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return version != currentVersion
        }
        throw SPFVersionError.invalidResponse
    }
     */
    
    public class func remoteDeviceToken(from tokenData: Data) -> String {
        let tokenParts = tokenData.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        return token
    }
    
}
