
import Foundation

/**
 Closure can be used whenever a closure without params and returned values is needed
 */
public typealias VoidClosure = () -> Void

/**
 Closure can be used whenever a closure with a Bool param is needed
 */
public typealias BoolClosure = (Bool) -> Void

/**
 Closure can be used whenever a closure with a Int param is needed
 */
public typealias IntClosure = (Int) -> Void

/**
 Closure can be used whenever a closure with a Float param is needed
 */
public typealias FloatClosure = (Float) -> Void

/**
 Closure can be used whenever a closure with a Double param is needed
 */
public typealias DoubleClosure = (Double) -> Void

/**
 Closure takes in an optional error
 */
public typealias ErrorClosure = ((Error?) -> Void)

/**
 Closure can be used whenever a closure with a Result and no returns is needed
 */
public typealias CompletionClosure<T, E:Error> = (Result<T, E>) -> Void
