//
//  SPFHTTPVersion.swift
//  SPFoundation
//
//  Created by Federico on 24/03/2020.
//

import Foundation

/// A structure representing a HTTP version.
public struct SPFHTTPVersion: Equatable {
    /// Create a HTTP version.
    ///
    /// - Parameter major: The major version number.
    /// - Parameter minor: The minor version number.
    public init(major: Int, minor: Int) {
        self._major = UInt16(major)
        self._minor = UInt16(minor)
    }

    private var _minor: UInt16
    private var _major: UInt16

    /// The major version number.
    public var major: Int {
        get {
            return Int(self._major)
        }
        set {
            self._major = UInt16(newValue)
        }
    }

    /// The minor version number.
    public var minor: Int {
        get {
            return Int(self._minor)
        }
        set {
            self._minor = UInt16(newValue)
        }
    }

}
