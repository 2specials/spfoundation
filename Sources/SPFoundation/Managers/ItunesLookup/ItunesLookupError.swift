
import Foundation

/// Enumerates all potentials errors that ItunesLookupManager can handle.
public enum ItunesLookupError: LocalizedError {
    /// Error retrieving trackId as the JSON does not contain a 'trackId' key.
    case appStoreAppIDFailure
    /// Error retrieving App Store data as JSON results were empty. Is your app available in the US? If not, change the `countryCode` variable to fix this error.
    case appStoreDataRetrievalEmptyResults
    /// Error retrieving App Store data as an error was returned.
    case appStoreDataRetrievalFailure(underlyingError: Error?)
    /// Error parsing App Store JSON data.
    case appStoreJSONParsingFailure(underlyingError: Error)
    /// The version of iOS on the device is lower than that of the one required by the app version update.
    case appStoreOSVersionUnsupported
    /// Error retrieving App Store verson number as the JSON does not contain a `version` key.
    case appStoreVersionArrayFailure
    /// The `currentVersionReleaseDate` key is missing in the JSON payload.
    case currentVersionReleaseDate
    /// One of the iTunes URLs is malformed.
    case malformedURL
    /// Please make sure that you have set a `Bundle Identifier` in your project.
    case missingBundleID
    /// No new update available.
    case noUpdateAvailable

    /// The localized description for each error handled by ItunesLookupManager.
    public var localizedDescription: String {
        switch self {
        case .appStoreAppIDFailure:
            return "\(ItunesLookupError.perfix) Error retrieving trackId as the JSON does not contain a `trackId` key."
        case .appStoreDataRetrievalFailure(let error?):
            return "\(ItunesLookupError.perfix) Error retrieving App Store data as an error was returned\nAlso, the following system level error was returned: \(error)"
        case .appStoreDataRetrievalEmptyResults:
            return "\(ItunesLookupError.perfix) Error retrieving App Store data as the JSON results were empty. Is your app available in the US? If not, change the `countryCode` variable to fix this error."
        case .appStoreDataRetrievalFailure(.none):
            return "\(ItunesLookupError.perfix) Error retrieving App Store data as an error was returned."
        case .appStoreJSONParsingFailure(let error):
            return "\(ItunesLookupError.perfix) Error parsing App Store JSON data.\nAlso, the following system level error was returned: \(error)"
        case .appStoreOSVersionUnsupported:
            return "\(ItunesLookupError.perfix) The version of iOS on the device is lower than that of the one required by the app version update."
        case .appStoreVersionArrayFailure:
            return "\(ItunesLookupError.perfix) Error retrieving App Store verson number as the JSON does not contain a `version` key."
        case .currentVersionReleaseDate:
            return "\(ItunesLookupError.perfix) The `currentVersionReleaseDate` key is missing in the JSON payload."
        case .malformedURL:
            return "\(ItunesLookupError.perfix) One of the iTunes URLs is malformed."
        case .missingBundleID:
            return "\(ItunesLookupError.perfix) Please make sure that you have set a `Bundle Identifier` in your project."
        case .noUpdateAvailable:
            return "\(ItunesLookupError.perfix) No new update available."
        }
    }

    /// An easily identifiable prefix for all errors thrown by ItunesLookupManager.
    private static var perfix: String {
        return "[ItunesLookupManager Error]"
    }
}
