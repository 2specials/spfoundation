import Foundation

/**
 Protocol is the base for a viewModel that supports asyncronous data fetching.
 */
public protocol FetcherViewModelProtocol {
    
    /**
     Func starts the fetching of data. Once done `viewModelsDidUpdateClosure` must be triggered.

     The logic is that the viewModels implements this method, fetches the data and when done it triggers the closure.
     ViewController, or anything interested on observing viewModels changes, gives a value to the closure to react accordingly.

     - note: base implementation does nothing
     */
    func fetchData()

    /**
     Triggered once the viewModels have been updated after a data fetch.

     - note: must be triggered on the main thread.
     */
    var viewModelsDidUpdateClosure: VoidClosure? { get set }

    /**
     Triggered if the data fetching fails for any reason.

     - note: must be triggered on the main thread
     */
    var viewModelDidFailedToUpdateClosure: ErrorClosure? { get set }
}

public extension FetcherViewModelProtocol {
    func fetchData() {}
}
