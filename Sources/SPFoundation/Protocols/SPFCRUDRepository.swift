
public protocol SPFCRUDRepository {
    
    associatedtype T
    
    func getAll() -> [T]
    func get(id: Int) -> T?
    func create(item:T) -> Bool
    func update(item:T) -> Bool
    func delete(item:T) -> Bool
    
}
