import Foundation

public protocol TableViewModelsHolderProtocol : class, FetcherViewModelProtocol {
    
    /**
     The section view model type
     */
    associatedtype SectionViewModelType:TableViewSectionViewModelProtocol

    /**
     Holds the view models
     */
    var viewModels:[SectionViewModelType] { get set }

    /**
     Holds the filtered view models. It's intended to act as a proxy for `viewModels`.

     - note: base implementation returns `viewModels`, override it to provide custom filtering
     */
    var filteredViewModels:[SectionViewModelType] { get }

    /**
     Returns the section view model at the given section

     - parameter section: the section's index.
     - returns: the section view model, if available, nil otherwise.
     */
    func tableViewSectionViewModel(atSection section:Int) -> SectionViewModelType?

    /**
     Returns the row view model at the given index path

     - parameter indexPath: the index path
     - returns: the row view model, if available, nil otherwise
     */
    func tableViewRowViewModel(at indexPath:IndexPath) -> SectionViewModelType.RowViewModelType?
}

public extension TableViewModelsHolderProtocol
{
    var filteredViewModels:[SectionViewModelType] { return self.viewModels }

    func tableViewSectionViewModel(atSection section:Int) -> SectionViewModelType? {
        return self.filteredViewModels[safe: section]
    }

    func tableViewRowViewModel(at indexPath:IndexPath) -> SectionViewModelType.RowViewModelType? {
        return self.filteredViewModels[safe: indexPath.section]?.rows[safe: indexPath.row]
    }
}

/**
 Protocol represents a tableview section. It holds the viewmodels to handle the contents of the section
 */
public protocol TableViewSectionViewModelProtocol
{
    associatedtype RowViewModelType
    associatedtype D

    /**
     List of row view models for the section
     */
    var rows:[RowViewModelType] { get }

    /**
     The section's title, if needed. It defaults to nil
     */
    var title:String? { get set }

    /**
     Here you can store anything you need for the section.
     It defaults to nil, override to provide a custom type and value.
     */
    var data:D? { get set }
}

public extension TableViewSectionViewModelProtocol
{
    var data: Void? {
        get { return nil }
        set { }
    }
    
    var title:String? {
        get { return nil }
        set { }
    }
}
