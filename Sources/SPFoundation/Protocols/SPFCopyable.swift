
import Foundation

public protocol SPFCopyable: Codable {
    var copy: Self? { get }
}

extension SPFCopyable {
    
    public var copy: Self? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return try? JSONDecoder().decode(Self.self, from: data)
    }
    
}
