import Foundation

/**
 Protocol is intended to be implemented by ViewModels that can provide a TableViewCell's height
 */
public protocol CellHeightProviderViewModelProtocol {
    /**
     The cell's height
     */
    var heightForCell: Float { get }

    /**
     The cell's height with a given max width

     - parameter width: the max width
     - returns: the height
     - note: defaults to `self.heightForCell`
     */
    func heightForCell(withMaxWidth width: Float) -> Float
}

public extension CellHeightProviderViewModelProtocol
{
    func heightForCell(withMaxWidth width: Float) -> Float { return self.heightForCell }
}

/**
 Protocol is intended to be adopted from ViewModels that handle the content of a TableView.
 - note: It comes with a base implementation for all the methods/properties that would suite most of the cases.
 */
public protocol TableViewModelProtocol: TableViewModelsHolderProtocol {
    /**
     The number of sections available
     */
    var numberOfSections: Int { get }

    /**
     If true the viewModel will provide support for an empty result cell when no results are available

     - note: defaults to `true`
     */
    var hasEmptyResultCell: Bool { get }

    /**
     The number of rows for the section at the given index

     - parameter section: the section index
     - returns: the number of rows
     */
    func numberOfRows(inSection section: Int) -> Int

    /**
     The height for the cell at the given indexPath

     - parameter indexPath: the indexPath
     - parameter maxWidth: the requested max width
     - returns: the height
     */
    func heightForRowAt(indexPath:IndexPath, maxWidth: Float?) -> Float
}

public extension TableViewModelProtocol {
    var hasEmptyResultCell:Bool { return true }

    var numberOfSections:Int {
        let numberOfSections = self.filteredViewModels.count
        guard numberOfSections == 0 && self.hasEmptyResultCell else {
            return numberOfSections
        }
        return 1
    }

    func numberOfRows(inSection section:Int) -> Int {
        let numberOfRows = self.filteredViewModels[safe: section]?.rows.count ?? 0
        guard numberOfRows == 0 && self.hasEmptyResultCell else {
            return numberOfRows
        }
        return 1
    }

    func heightForRowAt(indexPath:IndexPath, maxWidth: Float?) -> Float {
        guard let viewModel = self.tableViewRowViewModel(at: indexPath) else {
            return 0
        }

        guard let heightProvider = viewModel as? CellHeightProviderViewModelProtocol else { return 0 }
        guard let maxWidth = maxWidth else {
            return heightProvider.heightForCell
        }
        return heightProvider.heightForCell(withMaxWidth: maxWidth)
    }
}
